package sudokuconverter.converter;

import sudokuconverter.ImmutableSudoku;
import sudokuconverter.SudokuConverter;

/**
 *
 * @author Mattias
 */
public class LineConverter implements SudokuConverter {

	private static final String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String DIGITS = "123456789" + LETTERS + LETTERS.toLowerCase();

	@Override
	public String convert(ImmutableSudoku sudoku) {
		int size = sudoku.getBoxSize() * sudoku.getBoxSize();

		StringBuilder data = new StringBuilder();
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				int cell = sudoku.getCell(x, y);
				if (cell == 0) {
					data.append('.');
				} else {
					data.append(DIGITS.charAt(cell - 1));
				}
			}
		}

		return data.toString();
	}
}
