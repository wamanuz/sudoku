package sudokuconverter.converter;

import sudokuconverter.ImmutableSudoku;
import sudokuconverter.SudokuConverter;

/**
 * Algorithm 3: Exact Cover reduction (haskell)
 */
public class Impl3Converter implements SudokuConverter {

	private static final String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String DIGITS = "123456789" + LETTERS + LETTERS.toLowerCase();

	@Override
	public String convert(ImmutableSudoku sudoku) {
		int size = sudoku.getBoxSize() * sudoku.getBoxSize();

		StringBuilder data = new StringBuilder();
		data.append("tn1 Traditional " + sudoku.getBoxSize() + " -#").append(DIGITS.substring(0, size));
		for (int y = 0; y < size; y++) {
			data.append(" ");
			for (int x = 0; x < size; x++) {
				int cell = sudoku.getCell(x, y);

				if (cell == 0) {
					data.append('-');
				} else {
					data.append(DIGITS.charAt(cell - 1));
				}
			}
		}
        data.append('\n'); 

		return data.toString();
	}
}
