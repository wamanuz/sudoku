package sudokuconverter.converter;

import sudokuconverter.ImmutableSudoku;
import sudokuconverter.SudokuConverter;

/**
 * Algorithm 2: Pen-and-paper
 */
public class Impl2Converter implements SudokuConverter {

	@Override
	public String convert(ImmutableSudoku sudoku) {
		int size = sudoku.getBoxSize() * sudoku.getBoxSize();

		StringBuilder data = new StringBuilder();
		//data.append("[\n");
		//data.append("Sudoku=[");
		for (int y = 0; y < size; y++) {
			if (y != 0) {
				data.append(";\n");
			}
			for (int x = 0; x < size; x++) {
				int cell = sudoku.getCell(x, y);
				if (x != 0) {
					data.append(" ");
				}
				assert cell >= 0;
				data.append(cell);
			}
			//data.append(";\n");
			//data.append(" ");
		}
		//data.append("]");

		return data.toString();
	}
}
