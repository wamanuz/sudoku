package sudokuconverter.converter;

import sudokuconverter.ImmutableSudoku;
import sudokuconverter.SudokuConverter;

/**
 * Algorithm 5: SAT
 */
public class Impl5Converter implements SudokuConverter {

	@Override
	public String convert(ImmutableSudoku sudoku) {
		int size = sudoku.getBoxSize() * sudoku.getBoxSize();

		int width = (int) Math.log10(size) + 1;
		String space = "";
		for (int i = 0; i < width; i++) {
			space += " ";
		}

		StringBuilder data = new StringBuilder();
		data.append(sudoku.getBoxSize()).append(" ").
				append(sudoku.getBoxSize()).append('\n');
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				int cell = sudoku.getCell(x, y);

				if (x != 0) {
					data.append(' ');
				}

				int cellWidth = (int) Math.log10(cell == 0 ? 1 : cell) + 1;
				data.append(space.substring(0, width - cellWidth)).
						append(cell);
			}
			data.append('\n');
		}

		return data.toString();
	}
}
