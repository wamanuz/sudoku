package sudokuconverter.converter;

import sudokuconverter.ImmutableSudoku;
import sudokuconverter.SudokuConverter;

/**
 * Algorithm 6: Constraint Satisfaction Problem
 */
public class Impl6Converter implements SudokuConverter {

	@Override
	public String convert(ImmutableSudoku sudoku) {
		int size = sudoku.getBoxSize() * sudoku.getBoxSize();

		StringBuilder cluelines = new StringBuilder();
		int clues = 0;
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				int cell = sudoku.getCell(x, y);
				if (cell != 0) {
					clues++;
					cluelines.append(y + 1).append(' ')
							.append(x + 1).append(' ')
							.append(cell).append('\n');
				}
			}
		}
		StringBuilder sudokuData = new StringBuilder();
		sudokuData.append(size).append('\n');
		sudokuData.append(clues).append('\n');
		sudokuData.append(cluelines);

		return sudokuData.toString();
	}
}
