package sudokuconverter.loader;

import java.io.IOException;
import java.io.InputStream;
import sudokuconverter.SimpleSudoku;
import sudokuconverter.SudokuLoader;

/**
 *
 * @author Mattias
 */
public class LineLoader implements SudokuLoader<SimpleSudoku> {

	@Override
	public SimpleSudoku load(InputStream in) throws IOException {
		StringBuilder line = new StringBuilder();
		int c;
		while ((c = in.read()) != '\n') {
			if (c == -1) {
				return null;
			}
			line.append((char) c);
		}

		int n = (int) Math.sqrt(line.length());
		int[] data = new int[n * n];
		for (int i = 0; i < data.length; i++) {
			char v = line.charAt(i);
			if (v == '.') {
				data[i] = 0;
			} else if (v >= '0' && v <= '9') {
				data[i] = v - '0';
			} else if (v >= 'A' && v <= 'Z') {
				data[i] = v - 'A' + ('9' - '0' + 1);
			} else if (v >= 'a' && v <= 'z') {
				data[i] = v - 'a' + ('9' - '0' + 1) + ('Z' - 'A' + 1);
			} else {
				System.err.println(v);
				data[i] = -1;
			}
		}

		return new SimpleSudoku(data);
	}
}
