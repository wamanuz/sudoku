package sudokuconverter.loader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import sudokuconverter.SimpleSudoku;
import sudokuconverter.SudokuLoader;

/**
 *
 * @author Mattias
 */
public class DELoader implements SudokuLoader<SimpleSudoku> {

	private static final String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String DIGITS = "0123456789" + LETTERS.toLowerCase();

	@Override
	public SimpleSudoku load(InputStream in) throws IOException {
		StringBuilder line = new StringBuilder();

		int tab = 0;
		boolean skip = false;
		boolean started = false;

		int n = 36;
		int[] data = new int[n * n];
		int i = 0;
		for (; i < data.length;) {
			int v = in.read();
			if (v == -1) {
				if (!started) {
					return null;
				}
				break;
			}
			started = true;
			char c = (char) v;

			if (c == '\n') {
				skip = !skip;
				tab = 0;
				continue;
			}
			if (!skip) {
				if (c == '\t') {
					if (tab > 0 && tab % 2 == 0) {
						line.append('.');
						i++;
					}
					tab++;
				} else {
					line.append(c);
					data[i] = DIGITS.indexOf(c);
					assert data[i] > 0;
					i++;
					tab = 0;
				}
			}
		}

		//System.out.println();
		System.out.println(line.toString());

		n = (int) Math.sqrt(i);
		data = Arrays.copyOf(data, n * n);

		if (data.length == 0) {
			return null;
		}

		// :S
		if (n == 25) {
			for (int j = 0; j < data.length; j++) {
				if (data[j] != 0) {
					data[j] = data[j] - 9;
				}
			}
		}
		for (int j = 0; j < data.length; j++) {
			assert 0 <= data[j];
			assert data[j] < n;
		}

		System.out.print(i);
		System.out.print("\t");
		System.out.println((int) Math.sqrt(i));

		return new SimpleSudoku(data);
	}
}
