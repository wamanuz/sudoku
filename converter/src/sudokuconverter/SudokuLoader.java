package sudokuconverter;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Mattias
 */
public interface SudokuLoader<S extends ImmutableSudoku> {

	public S load(InputStream in) throws IOException;
}
