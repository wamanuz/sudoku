package sudokuconverter;

/**
 *
 * @author Mattias
 */
public interface ImmutableSudoku {

	public int getCell(int x, int y);

	public int getBoxSize();
}
