package sudokuconverter;

/**
 *
 * @author Mattias
 */
public class SimpleSudoku implements Sudoku {

	private int N, n;
	private final int[] data;

	public SimpleSudoku(int[] data) {
		assert data != null;

		this.N = (int) Math.sqrt(data.length);
		this.n = (int) Math.sqrt(N);
		assert data.length == N * N;
		assert N == n * n;

		this.data = new int[N * N];
		for (int i = 0; i < data.length; i++) {
			assert data[i] >= 0;
			assert data[i] <= N;

			this.data[i] = data[i];
		}
	}

	@Override
	public void setCell(int x, int y, int v) {
		assert v >= 0;
		data[x + N * y] = v;
	}

	@Override
	public int getCell(int x, int y) {
		return data[x + N * y];
	}

	@Override
	public int getBoxSize() {
		return n;
	}

	public String getCompactCode() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.data.length; i++) {
			sb.append(this.data[i]);
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return getCompactCode();
	}
}
