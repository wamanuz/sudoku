package sudokuconverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import sudokuconverter.converter.Impl2Converter;
import sudokuconverter.converter.Impl3Converter;
import sudokuconverter.converter.Impl5Converter;
import sudokuconverter.converter.Impl6Converter;
import sudokuconverter.converter.LineConverter;
import sudokuconverter.loader.DELoader;
import sudokuconverter.loader.LineLoader;

/**
 *
 * @author Mattias
 */
public class SudokuConverterMain {

	public static void main(String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage: source target path1 [path2 [path3 [...]]]");
			System.exit(0);
		}

		String source = args[0];
		String target = args[1];

		System.out.printf("Source: %s\n", source);
		System.out.printf("Target: %s\n", target);

		String[] paths = Arrays.copyOfRange(args, 2, args.length);

		System.out.printf("Paths: %s\n", Arrays.toString(paths));

		SudokuLoader<SimpleSudoku> loader = createLoader(source);
		SudokuConverter converter = createConverter(target);

		List<File> files = new LinkedList<File>();
		for (String path : paths) {
			File pf = new File(path);
			if (pf.isDirectory()) {
				files.addAll(Arrays.asList(pf.listFiles()));
			} else {
				files.add(pf);
			}
		}

		// Create destination directory
		File convertDir = new File("converted/" + target);
		convertDir.mkdirs();

		// Convert files
		System.out.print("Converting... ");
		System.out.flush();
		int counter = 0;
		System.out.println();
		for (File file : files) {
			System.out.println(file.getPath());
			InputStream in = new FileInputStream(file);
			//String outname = "puzzle_" + file.getName();
			for (int i = 0; ; i++, counter++) {
				// Load
				ImmutableSudoku sudoku = loader.load(in);
				if (sudoku == null) {
					break;
				}

				// Convert
				String data = converter.convert(sudoku);

				// Write
				String outname = "puzzle_" + file.getName() + "_" + i;
				FileWriter writer = new FileWriter(convertDir.getPath() + "/" + outname + ".txt");
				//writer.write("[");
				writer.write(data);
				//writer.write(" ");
				writer.close();
			}
			//writer.write("]");
			//writer.close();
		}
		System.out.println("done!");

		System.out.printf("Converted %d puzzles\n", counter);
	}

	private static SudokuConverter createConverter(String target) {
		Map<String, SudokuConverter> converters = new HashMap<String, SudokuConverter>();
		converters.put("impl2", new Impl2Converter());
		converters.put("impl3", new Impl3Converter());
		converters.put("impl5", new Impl5Converter());
		converters.put("impl6", new Impl6Converter());
		converters.put("line", new LineConverter());

		SudokuConverter converter = converters.get(target);
		if (converter == null) {
			throw new RuntimeException("Unknown converter: " + target);
		}
		return converter;
	}

	private static SudokuLoader<SimpleSudoku> createLoader(String source) {
		Map<String, SudokuLoader<SimpleSudoku>> converters =
				new HashMap<String, SudokuLoader<SimpleSudoku>>();
		converters.put("line", new LineLoader());
		converters.put("de", new DELoader());

		SudokuLoader<SimpleSudoku> loader = converters.get(source);
		if (loader == null) {
			throw new RuntimeException("Unknown loader: " + source);
		}
		return loader;
	}
}
