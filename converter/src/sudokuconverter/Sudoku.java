package sudokuconverter;

/**
 *
 * @author Mattias
 */
public interface Sudoku extends ImmutableSudoku {
  
    public void setCell(int x, int y, int v);
} 