package sudokuconverter;

/**
 *
 * @author Mattias
 */
public interface SudokuConverter {

	public String convert(ImmutableSudoku sudoku);
}
