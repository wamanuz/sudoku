/*
 * This is a JavaScript Scratchpad.
 *
 * Enter some JavaScript, then Right Click or choose from the Execute Menu:
 * 1. Run to evaluate the selected text (Ctrl+r),
 * 2. Inspect to bring up an Object Inspector on the result (Ctrl+i), or,
 * 3. Display to insert the result in a comment after the selection. (Ctrl+l)
 */

// max: 7
var n = 3

url = '/sudoku/eng/random.html?diff=5'
url = '/sudoku/'+n+'/eng/random.html'
url = '/sudoku/'+n+'/eng/random.html?diff=6'

var MAX = 100
var DUPMAX = 200

var N = n*n
var LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
var DIGITS = '123456789' + LETTERS + LETTERS.toLowerCase()

var dups = 0
var sudokus = []
function reqListener() {
    var doc = this.response

    var grid = doc.getElementsByClassName('grid')[0]

    var text = grid.innerHTML.split('</table>')[1]
    var difficulty = text.match(/Difficulty: (.+) \(\d+\)/)[1]

    var table = grid.children[0]
    var tbody = table.children[0]

    var sudoku = ''
    var showAlert = true
    for (var y = 0; y < N; y++) {
        var row = tbody.children[y]

        for (var x = 0; x < N; x++) {
            var cell = row.children[x].innerHTML

            if (cell === '&nbsp;') {
                cell = '.'
            } else {
                if (parseInt(cell) > DIGITS.length) {
                    console.log('Invalid cell value: '+cell)
                }
                cell = DIGITS.charAt(parseInt(cell)-1)
            }

            sudoku += cell
        }
    }

    var data = sudoku + '\t' + difficulty
    //console.log(data)
    
    next = sudokus.indexOf(data) == -1
    if (next) {
        sudokus.push(data)
    } else dups++

    scrape(next)
}

var xhr = new XMLHttpRequest();
xhr.onload = reqListener
               
var i = 0
function scrape(next) {
    if (i < MAX && dups < DUPMAX) {
        i++
        xhr.open('get', url)
        xhr.responseType = "document";
        xhr.send()
    } else {
        console.log(sudokus)
        document.body.innerHTML = sudokus.join('<br>')
    }
}

scrape(true)
