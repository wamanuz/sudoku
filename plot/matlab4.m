time = [0.11 0.84 5.12 29.02 47.24]
size = [195 150 149 189 95]
avg = time./size
avg = avg ./ avg(1)
x = [2 3 4 5 6]'
y = avg'
ft = fittype('exp1')
f = fit(x,y,ft)
plot(f,'r--',x,y)

fs = sprintf('f(x) = %f*exp(%fx)', f.a, f.b);

title('Algorithm 4: Exact Cover (Python)')
xlabel('size')
ylabel('time')

xlim([0 7])
legend('Location','NorthWest')

yl = ylim;
xl = xlim;
text(xl(1) + (xl(2)-xl(1))*0.02, yl(2)*0.8, fs)