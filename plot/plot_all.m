global L

L = 'r--';

h=figure('visible','off');
subplot(2,1,1)
plot_scaling_1

subplot(2,1,2)
plot_scaling_2

%saveas(h,'../graphs/scaling_1+2','epsc')
saveas(h,'../graphs/scaling_1+2_log','epsc')


h=figure('visible','off');
subplot(2,1,1)
plot_scaling_3

subplot(2,1,2)
plot_scaling_4

%saveas(h,'../graphs/scaling_3+4','epsc')
saveas(h,'../graphs/scaling_3+4_log','epsc')


h=figure('visible','off');
subplot(2,1,1)
plot_scaling_5

subplot(2,1,2)
plot_scaling_6

%saveas(h,'../graphs/scaling_5+6','epsc')
saveas(h,'../graphs/scaling_5+6_log','epsc')



h=figure('visible','off');
L = 'r--';plot_scaling_1
L = 'r-'; plot_scaling_2
L = 'k--';plot_scaling_3
L = 'k-'; plot_scaling_4
L = 'b--';plot_scaling_5
L = 'b-'; plot_scaling_6

legend('Brute-force', 'Pen-and-paper', 'Exact Cover (Haskell)', 'Exact Cover (Python)', 'SAT Reduction', 'CSP')
title('Time (all algorithms)')
%title('Scaling (all algorithms, beta variable)')
%ylabel('beta*n')

saveas(h,'../graphs/time_all','epsc')

break
ext = 'png'


h=figure('visible','off');
subplot(2,2,1)
plot_distribution_6(2);
%saveas(h,'../graphs/dist_6_n2',ext)
subplot(2,2,2)
plot_distribution_6(3);
%saveas(h,'../graphs/dist_6_n3',ext)
subplot(2,2,3)
plot_distribution_6(4);
%saveas(h,'../graphs/dist_6_n4',ext)
%subplot(2,2,4)
%plot_distribution_6(5);
%saveas(h,'../graphs/dist_6_n5',ext)

saveas(h,'../graphs/dist_6',ext)


h=figure('visible','off');
subplot(2,2,1)
plot_distribution_5(2);
%saveas(h,'../graphs/dist_5_n2',ext)
subplot(2,2,2)
plot_distribution_5(3);
%saveas(h,'../graphs/dist_5_n3',ext)
subplot(2,2,3)
plot_distribution_5(4);
%saveas(h,'../graphs/dist_5_n4',ext)
subplot(2,2,4)
plot_distribution_5(5);
%saveas(h,'../graphs/dist_5_n5',ext)

saveas(h,'../graphs/dist_5',ext)


h=figure('visible','off');
subplot(2,2,1)
plot_distribution_4(2,1);
%saveas(h,'../graphs/dist_4_n2',ext)
subplot(2,2,2)
plot_distribution_4(3,3);
%saveas(h,'../graphs/dist_4_n3',ext)
subplot(2,2,3)
plot_distribution_4(4,3);
%saveas(h,'../graphs/dist_4_n4',ext)
subplot(2,2,4)
plot_distribution_4(5,3);
%saveas(h,'../graphs/dist_4_n5',ext)

saveas(h,'../graphs/dist_4',ext)


h=figure('visible','off');
subplot(2,2,1)
plot_distribution_3(2,1);
%saveas(h,'../graphs/dist_3_n2',ext)
subplot(2,2,2)
plot_distribution_3(3,3);
%saveas(h,'../graphs/dist_3_n3',ext)
subplot(2,2,3)
plot_distribution_3(4,3);
%saveas(h,'../graphs/dist_3_n4',ext)
subplot(2,2,4)
plot_distribution_3(5,3);
%saveas(h,'../graphs/dist_3_n5',ext)

saveas(h,'../graphs/dist_3',ext)


h=figure('visible','off');
subplot(2,2,1)
plot_distribution_2(2,1);
%saveas(h,'../graphs/dist_2_n2',ext)
subplot(2,2,2)
plot_distribution_2(3,3);
%saveas(h,'../graphs/dist_2_n3',ext)
subplot(2,2,3)
plot_distribution_2(4,3);
%saveas(h,'../graphs/dist_2_n4',ext)
%subplot(2,2,4)
%plot_distribution_2(5,3);
%saveas(h,'../graphs/dist_2_n5',ext)

saveas(h,'../graphs/dist_2',ext)


h=figure('visible','off');
subplot(2,2,1)
plot_distribution_1(2,1);
%saveas(h,'../graphs/dist_1_n2',ext)
subplot(2,2,2)
plot_distribution_1(3,3);
%saveas(h,'../graphs/dist_1_n3',ext)
subplot(2,2,3)
plot_distribution_1(4,3);
%saveas(h,'../graphs/dist_1_n4',ext)
%subplot(2,2,4)
%plot_distribution_1(5,3);
%saveas(h,'../graphs/dist_1_n5',ext)

saveas(h,'../graphs/dist_1',ext)
