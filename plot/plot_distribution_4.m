function times=plot_distribution_4(n, d)
	times = load(sprintf('../algorithm4/n%dd%d.times', n, d));

	plot_distribution(times)

	title(sprintf('n=%d', n))
