function times=plot_distribution_3(n, d)
	times = load(sprintf('../algorithm3/n%dd%d.times', n, d));

	plot_distribution(times)

	title(sprintf('n=%d', n))
