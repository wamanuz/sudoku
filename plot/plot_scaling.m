function plot_scaling(times, sizes)
    global L

	% http://www.mathworks.se/help/matlab/math/example-curve-fitting-via-optimization.html
	function [estimates, model] = fitcurvedemo(xdata, ydata)
		% Call fminsearch with a random starting point.
		start_point = rand(1, 2);
		model = @(x) expfun(x, xdata, ydata);
		estimates = fminsearch(model, start_point);
	end
	% expfun accepts curve parameters as inputs, and outputs sse,
	% the sum of squares error for A*exp(-lambda*xdata)-ydata.
	function [sse] = expfun(params, xdata, ydata)
		A = params(1);
		lambda = params(2);
		FittedCurve = A .* exp(-lambda * xdata);
		ErrorVector = FittedCurve - ydata;
		sse = sum(ErrorVector .^ 2);
	end

	avg = times./sizes;
	%avg = avg ./ avg(2);
	x = [2:(1+length(times))]';
	y = avg';

	ft = fittype('exp1');                       % matlab
	f = fit(x,y,ft)                             % matlab
	%plot(f,'r--',x,y,'*')
	%hold on
	%plot(x,y,'*','markersize',10)
	%[estimates, model] = fitcurvedemo(x,y);    % octave
	xx = [2:0.1:(1+length(times))]';
	yy = f.a*exp(f.b*xx);                      % matlab
	%yy = 1*exp(f.b*xx);                         % matlab
    %yy = log(yy);
	%yy = estimates(1) * exp(-estimates(2)*xx); % octave
	semilogy(xx, yy, L)
	hold on
	%semilogy(x,y,'.','markersize',15)
	%hold off

	%legend('fitted curve', 'data')

	%fs = sprintf('f(x) = %s*exp(%fx)', f.a, f.b);                     % matlab
	%fs = sprintf('f(x) = %f*exp(%fx)', estimates(1), -estimates(2)); % octave

	xlabel('Size')
	ylabel('Time')

	xl = xlim;
	xlim([0 xl(2)+1])
	legend('Location','NorthWest')

	yl = ylim;
	xl = xlim;
	%text(xl(1) + (xl(2)-xl(1))*0.02, yl(2)*0.7, fs)
end
