time = [58 406 53053 53053*3 53053*8] / 1000
size = [20 20 20 20 20]
avg = time./size
avg = avg ./ avg(1)
x = [2 3 4 5 6]'
y = avg'
ft = fittype('exp1')
f = fit(x,y,ft)
plot(f,'r--',x,y)

fs = sprintf('f(x) = %f*exp(%fx)', f.a, f.b);

title('Algorithm 2: Pen-and-paper')
xlabel('size')
ylabel('time')

xlim([0 7])
legend('Location','NorthWest')

yl = ylim;
xl = xlim;
text(xl(1) + (xl(2)-xl(1))*0.02, yl(2)*0.8, fs)