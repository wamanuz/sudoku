function plot_scaling_4()
	n2 = load('../algorithm4/n2d1.times');
	n3 = load('../algorithm4/n3d3.times');
	n4 = load('../algorithm4/n4d3.times');
	n5 = load('../algorithm4/n5d3.times');

	times = [median(n2) median(n3) median(n4) median(n5)]
	s = [1 1 1 1];
	plot_scaling(times, s)

	title('Algorithm 4: Exact Cover Reduction (Python)')
