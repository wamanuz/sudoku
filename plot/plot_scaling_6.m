function plot_scaling_6()
	addpath('../algorithm6')

	global Times
	Times = 0;

	n2d1_times; n2 = Times;
	n3d3_times; n3 = Times;
	n4d3_times; n4 = Times;
	%n5d3_times; n5 = Times;

	rmpath('../algorithm6')

	times = [median(n2) median(n3) median(n4)]
	plot_scaling(times, [1 1 1])

	title('Algorithm 6: Constraint Satisfaction Problem')
