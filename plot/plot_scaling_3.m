function plot_scaling_3()
	n2 = load('../algorithm3/n2d1.times');
	n3 = load('../algorithm3/n3d3.times');
	n4 = load('../algorithm3/n4d3.times');
	n5 = load('../algorithm3/n5d3.times');

	times = [median(n2) median(n3) median(n4) median(n5)]
	s = [1 1 1 1];
	plot_scaling(times, s)

	title('Algorithm 3: Exact Cover Reduction (Haskell)')
