function plot_scaling_5()
	addpath('../algorithm8')

	global Times
	Times = 0;

	n2d1_times; n2 = Times;
	n3d3_times; n3 = Times;
	n4d3_times; n4 = Times;
	n5d3_times; n5 = Times;

	rmpath('../algorithm8')

	times = median([n2 n3 n4 n5]);
	s = ones(1, length(times));
	plot_scaling(times, s)

	title('Algorithm 5: SAT Reduction')
