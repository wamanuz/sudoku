function times=plot_distribution_2(n, d)
	times = load(sprintf('../algorithm2/n%dd%d.times', n, d));

	plot_distribution(times/1000)

	title(sprintf('n=%d', n))
