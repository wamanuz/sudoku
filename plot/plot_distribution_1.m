function times=plot_distribution_1(n, d)
	times = load(sprintf('../algorithm1/n%dd%d.times', n, d));

	plot_distribution(times)

	title(sprintf('n=%d', n))
