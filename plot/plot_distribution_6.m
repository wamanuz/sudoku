function Times=plot_distribution_6(n)
	addpath('../algorithm6')

	global Times
	Times = 0;

	if n == 2
		n2d1_times
	end
	if n == 3
		n3d3_times
	end
	if n == 4
		n4d3_times
	end
	if n == 5
		n5d3_times
	end

	rmpath('../algorithm6')

	plot_distribution(Times)

	title(sprintf('n=%d', n))
