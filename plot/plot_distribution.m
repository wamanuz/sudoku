function plot_distribution(x)
	addpath('kde')

	% http://en.wikipedia.org/wiki/Kernel_density_estimation
	%randn('seed',8192);
	%[h, fhat, xgrid] = kde(x, 4*length(x));
	%hold on;
	%plot(xgrid, fhat, 'linewidth', 2, 'color', 'black');

	%H = hist(x, 50)
	%scale = max(H)/max(fhat)

	hist(x, 150)
	%plot(xgrid, fhat*scale, 'linewidth', 2, 'color', 'red');

	val = median(x)
	yl = ylim();
	plot([val,val], [yl(1),yl(2)], 'linewidth', 2, 'color', 'red')
	hold on
	hist(x, 150)
	hold off

	xlabel('Time (s)')
	ylabel('Count')
	%hold off;

	xl = xlim();
	xlim([0 xl(2)])
