function plot_scaling_1()
	n2 = load('../algorithm1/n2d1.times');
	n3 = load('../algorithm1/n3d3.times');
	n4 = load('../algorithm1/n4d3.times');

	times = [median(n2) median(n3) median(n4)]
	s = [1 1 1];
	plot_scaling(times, s)

	title('Algorithm 1: Brute-force')
