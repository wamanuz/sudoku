function plot_scaling_2()
	n2 = load('../algorithm2/n2d1.times');
	n3 = load('../algorithm2/n3d3.times');
	n4 = load('../algorithm2/n4d3.times');

	times = [median(n2) median(n3) median(n4)] ./ 1000
	s = [1 1 1];
	plot_scaling(times, s)

	title('Algorithm 2: Pen-and-paper')
