from __future__ import division

import sys 
import string 
import time 

symbols = '123456789' + string.ascii_uppercase + string.ascii_lowercase 


def isValid(spuzzle, cellIndex, symIndex): 
    if symIndex >= limit: 
        return False 
    row = cellIndex // limit
    col = cellIndex % limit 
    box = row//boxsize*boxsize*limit + col//boxsize*boxsize
    
    for i in range(limit): 
        # Check row 
        if spuzzle[row*limit + i] == symbols[symIndex]: 
            return False
        # Check column 
        if spuzzle[col + i*limit] == symbols[symIndex]: 
            return False
        # Check box 
        if spuzzle[
            box + (i//boxsize*limit + i%boxsize)
           ] == symbols[symIndex]: 
            return False 
    return True 

def solve(puzzle): 
    """Solve by setting a digit to a value and moving on until either it is 
    solved or it reaches an invalid state. At that point it will backtrack."""
    
    # The current cell index and symbolindex 
    cellIndex = 0 
    # Skip to next cell that is not a clue 
    while cellIndex < len(puzzle)-1 and puzzle[cellIndex] != '.': 
        cellIndex += 1 
    symIndex = 0 

    # The puzzle we do changes in. Since we do not want to change 
    # the given clues we need to keep a backup. Since strings are immutable 
    # we convert it to a char array. 
    spuzzle = list(puzzle)
    del spuzzle[-1] # Remove the trailing newline 

    global limit, boxsize 

    # The number of symbols is the square root of the length. It is 
    # also garanteed to be an integer by the generator we used. 
    limit = int(len(spuzzle) ** (1/2))

    # The boxsize is the square root of row length 
    boxsize = int(limit ** (1/2))  

    while cellIndex < len(puzzle)-1: # -1 for newline  
        if isValid(spuzzle, cellIndex, symIndex): 
            # Placing symbol symIndex is valid, proceed to next cell. 
            spuzzle[cellIndex] = symbols[symIndex]
            cellIndex += 1 
            # Skip to next cell that is not a clue 
            while cellIndex < len(puzzle)-1 and puzzle[cellIndex] != '.': 
                cellIndex += 1 

            symIndex = 0 
        else:
            symIndex += 1 
        if symIndex >= limit:  
            # The symIndex has exceeded the number of symbols used in game 
            # => the current solution is invalid. Backtrack. 
            spuzzle[cellIndex] = '.' 
            cellIndex -= 1 

            # Skip to previous cell that is a clue  
            while puzzle[cellIndex] != '.': 
                cellIndex -= 1 
            # symIndex is the currently placed symbol +1 (since it is invalid) 
            try: 
                symIndex = symbols.index(spuzzle[cellIndex])+1 
            except:  
                for i in range(limit): 
                    print(spuzzle[i*limit:(i+1)*limit])
                print(cellIndex, symIndex, symbols) 
                raise

    return ''.join(spuzzle) 
    

def main(): 
    """Solves Sudoku puzzles by brute-force"""
    if len(sys.argv) < 2: 
        print("Usage: python3 brute-force.py file file file...") 
        return 
    
    files = sys.argv[1:]
    for fname in files: 
        with open(fname) as f, open(fname+'.solved1', 'w') as s: 
            for line in f: 
                t = time.clock() 
                solved = solve(line) 
                s.write("%s %f\n" % (solved, time.clock()-t)) 
                s.flush() 
                print("%s %f" % (solved, time.clock()-t)) 


if __name__ == '__main__': 
    main() 


