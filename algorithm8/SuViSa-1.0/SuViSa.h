/* Copyright (C) 2005 G.P. Halkes
   Licensed under the Academic Free License version 2.0*/
#ifndef SUDOKUSOLVE_H
#define SUDOKUSOLVE_H

#include <stdlib.h>

extern int *positions;
extern int totalSize, totalPositions, smallSizeX, smallSizeY, maxValue,
	positionsNotZero;
void fatal(const char *fmt, ...);
void *safe_malloc(size_t size);
void readInput(void);
int readResult(void);
#endif
