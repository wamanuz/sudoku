/* Copyright (C) 2005 G.P. Halkes
   Licensed under the Academic Free License version 2.0*/
%{
#include <assert.h>
#include "SuViSa.h"
	
typedef enum {
	SMALL_SIZEX,
	SMALL_SIZEY,
	POSITION
} ReadNext;
static ReadNext readNext = SMALL_SIZEX;
static int positionsRead = 0;
%}

%option noyywrap
%%
^#.*\n			/* Skip comments */
[ \t\n]			/* Skip ws */
[0-9]+			{
					switch (readNext) {
						case SMALL_SIZEX:
							smallSizeX = atoi(yytext);
							if (smallSizeX <= 0)
								fatal("X dimension of small block must be > 0\n");
							readNext++;
							break;
						case SMALL_SIZEY:
							smallSizeY = atoi(yytext);
							if (smallSizeY <= 0)
								fatal("Y dimension of small block must be > 0\n");
							readNext++;
							totalSize = maxValue = smallSizeX * smallSizeY;
							totalPositions = totalSize * totalSize;
							positions = safe_malloc(totalPositions * sizeof(int));
							break;
						case POSITION:
							if (positionsRead == totalPositions)
								fatal("Too many positions in file\n");
							positions[positionsRead] = atoi(yytext);
							if (positions[positionsRead] > maxValue || positions[positionsRead] < 0)
								fatal("Value for position out of range (%d)\n", positions[positionsRead]);
							if (positions[positionsRead] > 0)
								positionsNotZero++;
							positionsRead++;
							break;
						default:
							assert(0);
					}
				}
.				fatal("Error in input\n");

%%
void readInput(void) {
	positionsNotZero = 0;
	yylex();
	if (positionsRead != totalPositions)
		fatal("Not enough positions in file\n");
}
