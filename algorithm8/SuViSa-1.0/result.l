/* Copyright (C) 2005 G.P. Halkes
   Licensed under the Academic Free License version 2.0*/
%{
#include "SuViSa.h"
%}

%option noyywrap
%option prefix="res"
%%
^c.*\n			/* Skip comments */
^s.*\n			/* Skip result */
^v				/* Skip inital v */
^SAT			/* Skip inital SAT */
[ \t\n]			/* Skip ws */
-[0-9]+			/* Skip variables set to true */
[0-9]+			{
					int value = atoi(restext);
					if (value == 0)
						return 0;
					value--;
					positions[value % totalPositions] = value / totalPositions + 1;
				}
.				fatal("Error in input\n");

%%
int readResult(void) {
	int i;
	reslex();
	for (i = 0; i < totalPositions; i++)
		if (positions[i] <= 0 || positions[i] > maxValue)
			return -1;
	return 0;
}
