/* Copyright (C) 2005 G.P. Halkes
   Licensed under the Academic Free License version 2.0*/
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "SuViSa.h"

int *positions;
int totalSize, totalPositions, smallSizeX, smallSizeY, maxValue,
	positionsNotZero;

void fatal(const char *fmt, ...) {
	va_list args;
	
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
	exit(EXIT_FAILURE);
}

void *safe_malloc(size_t size) {
	void *retval;
	
	retval = malloc(size);
	if (retval == NULL)
		fatal("Could not allocate memory: %s\n", strerror(errno));
	
	return retval;
}

static int nC2(int i) {
	return (i * (i - 1)) / 2;
}

extern FILE *yyin, *resin;
int main(int argc, char *argv[]) {
	char tmpFile[10];
	char tmpFile2[10];
	char *childArgs[4];
	int outputFD;
	FILE *output;
	int nrOfBlocks, nrOfClauses, nrOfVariables;
	int h, i, j, k, x, y, offset;
	pid_t pid;
	int status;

	if (argc != 3) {
		fatal("Usage: sudokusolve <SAT-solver|-s> <problem>\n");
	}
	
	if ((yyin = fopen(argv[2], "r")) == NULL)
		fatal("Could not open input: %s\n", strerror(errno));
	
	readInput();
	
	strcpy(tmpFile, "outXXXXXX");
	if ((outputFD = mkstemp(tmpFile)) < 0)
		fatal("Could not open file for output: %s\n", strerror(errno));
	if ((output = fdopen(outputFD, "w")) == NULL)
		fatal("Could not fdopen file for output: %s\n", strerror(errno));
	
	nrOfVariables = totalPositions * maxValue;
	nrOfBlocks = (totalSize / smallSizeX) * (totalSize / smallSizeY);
	nrOfClauses = positionsNotZero + /* Unit clauses */
		(
			2 * totalSize + /* Rows + columns "all true" */
			2 * totalSize * nC2(totalSize) /* Binary "both false" for rows and columns */
		) * maxValue + /* Once for each possible value */
		(
			nrOfBlocks + /* Blocks "all true" */
			nrOfBlocks * nC2(nrOfBlocks) /* Binary "both false" for blocks */
		) * maxValue + /* Once for each possible value */
		totalPositions + /* All positions at least one value "all true" */
		totalPositions * nC2(maxValue); /* Binary "both false" clauses for choosing only one value per position */
	;
	fprintf(output, "p cnf %d %d\n", nrOfVariables, nrOfClauses);
	for (h = 0; h < maxValue; h++) { /* Per Value */
		for (i = 0; i < totalSize; i++) { /* Per row */
			offset = h * totalPositions + i * totalSize;
			for (j = 0; j < totalSize; j++) /* Each variable */
				fprintf(output, "%d ", j + offset + 1);
			fprintf(output, "0\n");
			for (j = 0; j < totalSize - 1; j++) /* Each combination of two variable */
				for (k = j + 1; k < totalSize; k++)
					fprintf(output, "-%d -%d 0\n", j + offset + 1, k + offset + 1);
		}
		for (i = 0; i < totalSize; i++) { /* Per column */
			offset = h * totalPositions + i;
			for (j = 0; j < totalSize; j++) /* Each variable */
				fprintf(output, "%d ", j * totalSize + offset + 1);
			fprintf(output, "0\n");
			for (j = 0; j < totalSize - 1; j++) /* Each combination of two variable */
				for (k = j + 1; k < totalSize; k++)
					fprintf(output, "-%d -%d 0\n", j * totalSize + offset + 1, k * totalSize + offset + 1);
		}
		for (x = 0; x < (totalSize / smallSizeX); x++) {
			for (y = 0; y < (totalSize / smallSizeY); y++) {
				offset = h * totalPositions + y * smallSizeY * totalSize + x * smallSizeX;
				for (j = 0; j < maxValue; j++) {
					fprintf(output, "%d ", j % smallSizeX + totalSize * (j / smallSizeX) + offset + 1);
				}
				fprintf(output, "0\n");
				for (j = 0; j < maxValue - 1; j++) /* Each combination of two variable */
					for (k = j + 1; k < maxValue; k++)
						fprintf(output, "-%d -%d 0\n", j % smallSizeX + totalSize * (j / smallSizeX) + offset + 1, k % smallSizeX + totalSize * (k / smallSizeX) + offset + 1);
			}
		}
	}
	for (i = 0; i < totalPositions; i++) { /* For each position */
		for (j = 0; j < maxValue; j++)
			fprintf(output, "%d ", i + j * totalPositions + 1);
		fprintf(output, "0\n");
		for (j = 0; j < maxValue - 1; j++) /* Each combination of two variable */
			for (k = j + 1; k < maxValue; k++)
				fprintf(output, "-%d -%d 0\n", i + j * totalPositions + 1, i + k * totalPositions + 1);	
	}
	for (i = 0; i < totalPositions; i++)
		if (positions[i] != 0)
			fprintf(output, "%d 0\n", (positions[i] - 1) * totalPositions + i + 1);
	fclose(output);
	
	if (strcmp(argv[1], "-s") == 0) {
		printf("SAT instance has been saved to %s\n", tmpFile);
		exit(0);
	}

	strcpy(tmpFile2, "outXXXXXX");
	if ((outputFD = mkstemp(tmpFile2)) < 0)
		fatal("Could not open file for output: %s\n", strerror(errno));

	if ((pid = fork()) == 0) {
		if (dup2(outputFD, 1) < 0)
			fatal("Could not set up redirect in child: %s\n", strerror(errno));
		close(outputFD);
		childArgs[0] = argv[1];
		childArgs[1] = tmpFile;
		childArgs[2] = tmpFile2;
		childArgs[3] = NULL;
		execvp(childArgs[0], childArgs);
		exit(1);
	} else if (pid < 0) {
		fatal("Could not fork: %s\n", strerror(errno));
	}
	close(outputFD);

	if (waitpid(pid, &status, 0) < 0)
		fatal("Error waiting for child to exit: %s\n", strerror(errno));

	unlink(tmpFile);
	if (!WIFEXITED(status)) {
		unlink(tmpFile2);
		fatal("SAT solver terminated abnormally\n");
	}
	status = WEXITSTATUS(status);
	if (status == 20) {
		unlink(tmpFile2);
		fatal("Puzzle can not be solved\n");
	} else if (status != 10) {
		unlink(tmpFile2);
		fatal("SAT-solver gave unknown as result\n");
	}
	if ((resin = fopen(tmpFile2, "r")) == NULL) {
		unlink(tmpFile2);
		fatal("Could not open output from sat-solver\n");
	}
	if (readResult() < 0) {
		fclose(resin);
		unlink(tmpFile2);
		fatal("Output from SAT-solver was not in the correct format\n");
	}
	fclose(resin);
	unlink(tmpFile2);
	for (y = 0; y < totalPositions; y += totalSize) {
		for (x = 0; x < totalSize; x++) {
			printf("%4d ", positions[x + y]);
		}
		printf("\n");
	}
	return 0;
}
