% Title: Sudoko [sic] solvers 
% Theme: Algorithms 
% Subject: In principle
% Sudoku can be solved using a brute-force algorithm but it is more interesting
% to find an efficient sudoku lover. The task is to study and implement some
% sudoku solving algorithms, and hopefully to design your own solution or
% variation. The algorithms should be tested and benchmarked agains sudoku
% instances of varying difficulty.

% Konstig klass för att få \subtitle 
\documentclass[a4paper,abstracton]{scrartcl}

\usepackage[english]{babel} 
\usepackage[utf8]{inputenc}

\usepackage[titletoc,toc,title]{appendix}

\usepackage{amsmath} 
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{moreverb}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{rotating}
\usepackage{seqsplit}

\usepackage{url}
\usepackage{lipsum}

\usepackage[colorinlistoftodos]{todonotes} 
\usepackage[yyyymmdd]{datetime} %FIXME 
\renewcommand{\dateseparator}{-}

% To decrease the spacing between sections and regular text.
\usepackage{titlesec} 
\titlespacing\section{0pt}{12pt plus 4pt minus 2pt}{-6pt plus 2pt minus 2pt} 
\titlespacing\subsection{0pt}{12pt plus 4pt minus 2pt}{-6pt plus 2pt minus 2pt} 
\titlespacing\subsubsection{0pt}{12pt plus 4pt minus 2pt}{-6pt plus 2pt minus 2pt}


\title{Scaling of popular Sudoku solving algorithms} 
\subtitle{DD143X: Degree Project in Computer Science}

\author{ 
 \begin{tabular}{l r}
  \textbf{Authors}      &                  \\ \hline 
  \rule{0pt}{3ex} Markus Videll & \url{<mvidell@kth.se>}   \\ 
  \,\,Mattias Palo  & \url{<mpalo@kth.se>}     \\
                &                          \\
  \textbf{Supervisor}   &                  \\ \hline \rule{0pt}{3ex} 
  Alexander Kozlov   & \url{<akozlov@csc.kth.se>}  \\
                &                          \\
  \textbf{Examiner}     &                  \\ \hline \rule{0pt}{3ex}
  Karl Meinke   & \url{<karlm@csc.kth.se>} \\
  \,\,Örjan Ekeberg & \url{<orjan@csc.kth.se>} \\
                &                          \\
  \textbf{University}   & \textbf{School} \\ \hline \rule{0pt}{3ex}
  KTH & CSC
 \end{tabular} 
}


\begin{document} 
\parindent = 0pt 
\parskip = \baselineskip

\maketitle 
\thispagestyle{empty}

\pagebreak 

\begin{abstract} 
In this bachelor thesis we study 6 $n^2 \times n^2$ popular Sudoku solving algorithms, 
found through Google, to find the algorithm that has the slowest growth. The algorithms we 
tested are: brute-force, a pen-and-paper method, two exact cover reductions 
in Python and Haskell, a SAT reduction, and a constraint satisfaction algorithm. 
The algorithms were tried by solving Sudoku puzzles of sizes 
$n = \{2, 3, 4, 5\}$ where we measured the solution time. 
We conclude that the slowest growing algorithm, by far, is the SAT reduction, 
followed by the exact cover reductions. Brute-force grows, unsurprisingly, 
the fastest. 
\end{abstract}

% - - - - - - - - 
\pagebreak 
\tableofcontents 
\pagebreak 
% - - - - - - - -

\section{Statement of collaboration}
\begin{itemize} 
 \item Markus has written most of this report and ran the algorithms. 
 \item Mattias wrote most of the converters, prepared the algorithms and 
       created the plots. 
 \item Remaining work have been done together. 
\end{itemize}


\section{Introduction} 
Sudoku puzzles are a popular time sink, similar to crosswords. People 
solve them while riding the train, or are waiting for their dentist appointment. 
There is a much more serious side to it. There are several competitions 
in Sudoku solving including The World Sudoku 
Championship\cite{wsc}. There are even claims that solving Sudoku puzzles 
have a positive effect on brain power\cite{sudoku_brain}, though there are 
conflicting reports\cite{sudoku_nobrain}.

In this report we have chosen to study 6 popular $n^2 \times n^2$ Sudoku solving 
algorithms and we are going to examine how fast each algorithm scales  as the 
Sudoku puzzle grows. These values will then be plotted in a graph and the 
exponential function approximated. The algorithms will then be compared to each 
other to see which one exhibits the slowest growth. This report does not study 
properties other than solution time, e.g. memory usage. 

%TODO: Källor och något om KTH och kexjobb.


\section{Background}
\subsection{Terminology} \begin{description}
 \item \textbf{Box}: \hfill \\
  A region that no symbols can be in at the same time. It is a $3 \times 3$
  square in the standard $9 \times 9$ puzzle.
 \item \textbf{Cell}: \hfill \\
  One of the individual squares that can be a symbol, most often digits between $1-9$.
 \item \textbf{Clue/Given}: \hfill \\
  One of the given digits that you are not allowed to change. They are the
  clues for you to use when solving the puzzle.
 \item \textbf{Symbol}: \hfill \\
  The symbol that is put into a cell. In the standard $9 \times 9$ puzzle the 
  symbols are commonly the digits $1-9$. In bigger puzzles letters may be used
  as well as digits. There are $n^2$ symbols in a $n^2 \times n^2$ Sudoku puzzle. 
 \item \textbf{Ambiguous puzzle}: \hfill \\
  A puzzle with more than one solution.
 %\item \textbf{Minimal puzzle}: \hfill \\
 % A puzzle that if you remove a clue, the puzzle becomes ambiguous.
\end{description}


\subsection{Sudoku} 
\begin{figure}[h!]
 \centering 
 \begin{subfigure}[h]{0.4\textwidth}
  \includegraphics[width=\textwidth]{figures/sudoku.png}
 \end{subfigure}
 \quad
 \begin{subfigure}[h]{0.4\textwidth}
  \includegraphics[width=\textwidth]{figures/sudoku_solution.png}
 \end{subfigure}
 \caption{
  A standard $9 \times 9$ Sudoku board. To the left is the original board and 
  to the right is its solution in red\cite{sudoku_figures}. 
 }
 \label{fig:sudoku}
\end{figure}
Sudoku is a game with simple rules. The standard game is a $9 \times 9$
board with $3 \times 3$ boxes. The board is initially filled with a number
of clues to get you started, and your goal is to fill all cells with the
digits $1-9$ so that each number only occurs once in each row, column,
and box, until there are no empty cells left. See figure~\ref{fig:sudoku} for an 
example puzzle. 

As stated above, the most common boards are $9 \times 9$, but it is actually
possible to create boards with size $n \times n$, where $n$ is the width
in cells. It is important to note that if $n$ is not a squared number,
the smaller boxes will not be square. That is not a big issue when solving by hand,
but when we are searching for Sudoku solving algorithms we have limited
our searching and testing to puzzles with dimension $n^2 \times n^2$.

\begin{figure}[H] 
 \vspace{4pt}
 \centering
 \includegraphics[width=0.30\textwidth]{figures/sudoku_6x6.png}
 \caption{
   A $6 \times 6$ Sudoku puzzle\cite{sudoku_small}, commonly referred to as
   ``Kids Sudoku''. Notice that the boxes are not square when $n$ is not a 
   square number.
 }
\end{figure}


\subsection{Algorithms} 
\emph{
 In this section we describe the basic idea behind selected algorithms shown in 
 Table~\ref{table:alg}. Main principles are broadly explained and only some 
 implementation details are outlined here. 
}

\begin{table}[h]
\centering 
\begin{tabular}{c | l | l | l}
\textbf{\#} & \textbf{Algorithm} & \textbf{Language} & \textbf{Reference} \\ \hline 
1 & Brute-force   & Python        & \ref{subsec:brute} \\
2 & Pen-and-Paper & Matlab        & \cite{a2} \\
3 & Exact Cover Reduction   & Haskell       & \cite{a3} \\
4 & Exact Cover Reduction   & Python        & \cite{a4} \\
5 & SAT Reduction + MiniSat & C             & \cite{a5,minisat} \\
6 & Constraint Satisfaction Problem & Java & \cite{a6} \\
\end{tabular}
\caption{The selected algorithms} \label{table:alg} 
\end{table}


\subsubsection{Brute-force method} 
To solve a Sudoku puzzle using a brute-force method, one could do that by
looping through each cells, one by one, and picking a number between 1 to
9. This could be done either in a specified sequential or a random order. If
an invalid state is reached, i.e. there is a collision of symbols,
it will backtrack and make a new choice. By doing this exhaustive search you
are guaranteed a solution to the puzzle, provided there is one\cite{brute_force}.


\subsubsection{A Pen-and-Paper Algorithm}
A simple algorithm that is often used when solving by hand. 
Each cell has a list of potential candidates that starts with all possible symbols. 
By checking the row, column, and box, the conflicting values are removed 
from this list. When there is only one candidate left, that cell is set\cite{a2}.

This however is not enough to guarantee a solution, and the algorithm may come
to a halt even when there are unfilled cells. When this happen the algorithm
will choose the cell with the fewest possible candidates and pick one candidate at random. 
It will then continue doing logical deductions until it either finds a correct 
solution or an invalid state, in which it will backtrack\cite{a2}.


\subsubsection{Exact Cover Reduction}
Exact Cover is one of the first known NP-complete problems and was
proven NP-complete by Richard Karp in 1972. You are   
given a family of sets, $C$, and the problem is to find a subset, 
$C' \subseteq C$, so that each element in the sets in $C$ appears exactly in 
one member of the sets in $C'$\cite{karp}. Sudoku can be reduced to Exact Cover 
and, since $n^2 \times n^2$ Sudoku itself is NP-complete\cite{sudoku_np}, it 
can be reduced in polynomial time. % Fixme \cite{}? 

The reduction is done by creating a big matrix $C$, where each row is $C'$. 
Since this is a big matrix it is easier to represent the elements with indicator 
variables. A $1$ means that the element is present in this subset, and 
a $0$ means that it is not\cite{ams_exact_cover}.

For a standard $9 \times 9$ Sudoku board, the matrix will have $4 \cdot 81$
columns. The first 81 columns is a unique identifier for each cell. For
the Exact Cover to be solved, exactly one row for a given cell will be
chosen. The next 81 columns represents the row index of that cell. 
The last 162 columns is the similarly representing the columns and boxes of
the Sudoku puzzle\cite{ams_exact_cover}.
For example, a one in column 82 means that the cell in the first row is set to 1. 
A one in column 83 means that the cell in first row is set to 2, etc. 

%\begin{center} 
%TODO: A describing image? 
%\end{center}

The clues in the Sudoku puzzle will each be represented by a single row in
the matrix. Since each cell has a unique ID and there is only be one row
of them, these rows are forced to be a part of a solution.

But each empty cell will have to be represented by $n^2$ rows, one for each
symbol. The dimensions of the final matrix is given by 
$$(\text{number of clues } + 9 \cdot \text{number of empty cells}) \times 4 \cdot 81$$ 
for a standard $9 \times 9$ Sudoku board.

The Exact Cover problem is then solved by Donald Knuth's Algorithm X
with Dancing links\cite{ams_exact_cover}.


\subsubsection{SAT Reduction}
The Boolean Satisfiability Problem (SAT) is another popular NP-complete problem, and 
it is one of the most researched problems. Because of this there are several 
SAT solvers out there, and there are even competitions to see who can create the 
fastest\cite{satcomp}.

The reduction works by dividing the problem into the following rules: 
\begin{enumerate}
 \item Each row must contain each symbol once 
 \item Each column must contain each symbol once 
 \item Each box must contain each symbol once 
 \item Each cell in the puzzle can contain only one number 
\end{enumerate}

The program then formulate clauses, where each variable is a symbol in the 
puzzle. 
$$
(a \vee b \vee ... \vee x) \wedge (\neg a \vee \neg b) \wedge ... \wedge 
(\neg a \vee \neg x) \wedge (\neg b \vee \neg c) \wedge ... \wedge 
(\neg b \vee \neg x) \wedge ... \wedge (\neg w \vee \neg x)
$$
This formula means that only one of the symbols $a$ to $x$ can 
be true. The formula is used for each cell, row, column, and box. The given 
clues will be encoded by appending clauses consisting of a single variable corresponding 
to that number in that cell\cite{a5}. 

The reduction we used is only a reduction and does not solve the formula that is 
outputted. We used MiniSat which is currently one of the best solvers\cite{minisat}.


\subsubsection{Constraints Satisfaction Problem}
A Constraints Satisfaction Problem (CSP) is a problem that can be described 
as a set of variables, domains for the variables, and constraints. Sudoku
can easily be represented as a CSP. Each cell is a variable, and the 
domain is all the used symbols, $1-9$ in a standard Sudoku puzzle.
The constraint that each cell can not be the same as any other 
cell in each row, column, or box\cite{a6}. 

The program then solves the puzzle by making a choice, and then backtracks 
if it encounters an invalid situation. This works better than a simple 
brute-force as it also does a forward checking to constrain the variables 
domains\cite{a6}.

This implementation also provides two additional heuristics for harder problems. 
The Minimum-Remaining-Values that selects which variable to be assigned next, 
and Least-Constraining-Values which selects what value that should be 
assigned\cite{a6}. 


\section{Method} 
\subsection{Selecting algorithms} 
For the comparison study, 6 different implementations where selected 
among $n^2 \times n^2$ Sudoku solvers found on Google. Google was used 
because we wanted to find popular algorithms. Since scaling is independent 
of the language used in the implementation we did not hesitate to chose 
algorithms in different languages. For a list over selected algorithms, 
see Table~\ref{table:alg}.  


\subsection{Generating Sudoku puzzles} 
To test the algorithms we needed unambiguous puzzles in different sizes. We could 
not generate our own puzzles because of time constraints so we had to find 
already generated puzzles. 

All Sudokus we used are generated by the same program and are presented on the 
same website\cite{generator}. This program is able to generate Sudokus of 
different sizes and difficulties, though we noted that the bigger 
the generated Sudoku is, the easier and more clues they have. 

We downloaded puzzles with the sizes $n = \{2, 3, 4, 5\}$ and difficulty ``Easy'', 
and got about $150-200$ puzzles for each size. 

The ``Easy'' difficulty was chosen to guarantee uniform conditions for 
tests in cases for all sizes. Because of time constraints we also needed the algorithm 
to finish in a reasonable time. 


\subsection{Preparing the algorithms} 
Before testing the algorithms we made some small changes to them. Since they 
represented the Sudoku puzzles differently, we created a converter for 
almost all of them. On some algorithms we also made changes so that you could input 
a file with puzzles and it would solve all of them, and others would be run together with 
a bash script that looped over all puzzles. The solution would be printed to a separate 
file together with the solution time. 


\subsection{Running the algorithms} 
We used one of our own computers to run the algorithms, and it would stay on during 
the night. To speed up the testing we ran several algorithms at the same time.
Since no algorithm makes use of multiple cores this was deemed to not interfere with 
the scaling of each algorithm so long as the CPU load stayed below 100\%. 

\begin{table}[h]
\centering 
\begin{tabular}{c | l | l | l}
\textbf{\#} & \textbf{Algorithm} & \textbf{Compiler/Interpreter} & \textbf{Version} \\ \hline
1 & Brute-force                     & PyPy          & 2.2.1    \\
2 & Pen-and-Paper                   & GNU Octave    & 3.8.0    \\
3 & Exact Cover Reduction           & GHC           & 7.6.3    \\
4 & Exact Cover Reduction           & PyPy          & 2.2.1    \\
5 & SAT Reduction + MiniSat         & GCC           & 4.8.2    \\ 
6 & Constraint Satisfaction Problem & OpenJDK       & 1.7.0\_51 \\
\end{tabular}
\caption{
 A list of the different compilers and interpreters we used to test the algorithms. 
}
\end{table}

The computer we used is an x86\_64 machine with Arch Linux as operating system. 
It has 4 physical CPU cores but uses Hyper-threading to get two threads on each core. Each 
core is clocked at about 3.7GHz and it has 16GB of RAM. 
For more hardware information, see Appendix~\ref{sec:hw}. 


\subsection{Plotting the data}
The data was gathered and plotted using Matlab. The plotted values are 
the median value of all puzzles in that size, which were chosen to eliminate 
extreme cases. We used Matlab to approximate the growth rate as an exponential function, 
$f(n) := \alpha\exp(\beta n)$, between each data point. The exponential function was chosen 
because $n^2 \times n^2$ Sudoku is NP-complete and there exists no known algorithm to solve it 
in polynomial time. 

%Since the actual time to solve a puzzle is not important to us, the graphs were 
%normalized so $n = 3$ acts as a reference value. This way we can more easily 
%see how fast each algorithm grows. 


\section{Results} 
\subsection{Graphs}
\emph{
 Median values of solving times for selected algorithms are shown in the plots below. 
 Exponential fit $f(n)$ for the test data is presented in every graph for each algorithm. 
 Lastly, we present two combined graphs for easier comparison. 
 The first one depicts the scaling of each algorithm, $\beta n$, 
 and the second one is a combined graph of $f(n)$. 
 Distributions of solving times are shown in Appendix~\ref{sec:timedist}. 
}

%\vfill 
\begin{figure}[H]
 \centering 
 \includegraphics[height=\textheight/2]{graphs/scaling_1+2_log.eps}
 \includegraphics[height=\textheight/2]{graphs/scaling_3+4_log.eps}
\end{figure}
%\vfill  

\begin{figure}[H]
 \centering 
 \includegraphics[height=\textheight/2]{graphs/scaling_5+6_log.eps}
 \includegraphics[height=\textheight/2]{graphs/scaling_all.eps}
\end{figure}

\begin{figure}[H]
 \centering 
 \includegraphics[height=\textheight/2]{graphs/time_all.eps}
\end{figure}

\subsection{Interpretation}
The SAT reduction is the slowest growing algorithm, and the second slowest 
growing is the Exact Cover reductions. The Python version grows a bit faster 
than the Haskell version, as we can see on the approximated exponent in the 
function, but they are mostly the same. The Python version actually solves smaller 
puzzles faster than Haskell. 

The other algorithms, Pen-and-paper, CSP, and brute-force 
methods are all incredibly slow as the Sudoku puzzle grows. The Pen-and-paper and 
CSP were able to solve all problems with size $n = 4$ after a couple of days, but the 
brute-force had only managed to solve three puzzles after the same amount of time. 


\section{Discussion}
\subsection{Analysis} 
In this paper, we studied the scaling of 6 $n^2 \times n^2$ Sudoku solving algorithms. 
The results clearly show that the SAT reduction has the slowest growth rate of the 
studied algorithms, many times slower than Exact Cover in Python. We are not surprised that the SAT reduction 
was fast, it is as we mentioned a very studied problem, but we are surprised that 
it was so much faster than the other algorithms. The SAT reduction is also much less 
popular than the other algorithms like Exact Cover reductions and Pen-and-paper methods. 

The Pen-and-paper method and CSP algorithms were among the slowest in solution time, 
but that may be attributed to the language used. In scaling however they were not 
far away from the Exact Cover reductions. 

It is also important to note that just because the SAT reduction grows slowest, it 
does not mean that other algorithms are useless. There may be other scenarios where 
other algorithms are more suited, e.g. memory usage, puzzle generation, ease of implementation, and 
difficulty classification. For example when $n \le 3$ the solution time is below a 
second for almost all algorithms, and you should probably focus on other properties 
instead of scaling or solution time. Other properties were not studied at all in this report 
however. 


\subsection{Possible improvements} 
There are a few things that could have been done better. We used the difficulty 
classification from the website that generated the Sudoku puzzles, but we do not 
know exactly how they are classified as such. We also would have liked to use 
a harder classification than ``Easy'', but they were more difficult to find. 

Similarly, we should also have tried with more data points. While four or five data points is 
enough to see a drastic difference in solution time, it is not necessarily enough 
to accurately approximate the coefficients of the exponential function. To deal with this the 
algorithms could have been implemented in a faster language like C instead, and they may 
have been able to solve bigger puzzles. Because of time constraints we were not 
able to do this. 

The last problem is the implementations. They were found through Google and 
are created by ordinary people and are not necessarily optimal, except for MiniSat. 
We can see some difference for example between the Python and Haskell implementations. 
Python scales a bit faster than the Haskell implementation, even though they use the 
same method. This difference however is not that notable, and does not interfere with our results. 


\subsection{Conclusions}
We can draw the conclusions that the SAT reduction scales the slowest, followed by 
Exact Cover reductions. The brute-force method grows incredibly fast and should not 
be used for puzzles with large $n$ values. 



\pagebreak 
\begin{thebibliography}{9}

% ---- Introduction ----
\bibitem{wsc} World Puzzle Federation.  
$<$\url{http://www.worldpuzzle.org/championships/}$>$. 
Retrived 2014-03-31. 

\bibitem{sudoku_brain} Jeremy W. Grabbe. (2011). \emph{Sudoku and Working Memory Performance for Older Adults, Activities, 
Adaptation \& Aging} 35:3, 241-254, DOI: 10.1080/01924788.2011.596748

\bibitem{sudoku_nobrain} Adrian M. Owen \emph{et al}. \emph{Putting brain training to the test}
Nature 465, 775–778 (10 June 2010) doi:10.1038/nature09042

 % ---- FIGURES -----
\bibitem{sudoku_figures}
Both images from Wikipedia. The solution, CC-BY-SA, is provided by user Cburnett, 
$<$\url{http://en.wikipedia.org/wiki/File:Sudoku-by-L2G-20050714_solution.svg}$>$. 
Retrieved 2014-03-28.

\bibitem{sudoku_small}
$<$http://www.sudokuweb.org/easy-sudoku-6x6-for-kids/$>$
Retrieved 2014-03-28.
 % -----------------
% ---- ----

% ---- Algorithms ---- 
\bibitem{a2} \emph{Walter's tech blog}. 
$<$\url{http://waltertech426.blogspot.se/2013/07/matlab-sudoku-solver-in-fastest-way.html}$>$. 
Retrieved 2014-03-31. 

\bibitem{a3} Naur, Thorkil. \emph{Generalized solver by Thorkil Naur}. 
$<$\url{http://www.haskell.org/haskellwiki/Sudoku#Generalized\_solver}$>$.
Retrived 2014-03-31. 

\bibitem{a4} Rees, Gareth. \emph{Sudoku using 'exact cover' solver}.
$<$\url{http://codereview.stackexchange.com/questions/37415/sudoku-using-exact-cover-solver}$>$.
Retrived 2014-03-31. 

\bibitem{a5} G.P. Halkes. \emph{SuViSa}. $<$\url{http://os.ghalkes.nl/SuViSa.html}$>$. 
Retrived 2014-04-09. 

\bibitem{minisat} Eén, Niklas. Sörensson, Niklas. \emph{The MiniSat page}. 
$<$\url{http://minisat.se/}$>$. 
Retrived 2014-04-09. 

\bibitem{a6} Dadgar, Armon. \emph{A Constraint Satisfaction Solver (CSP) using Backtracking and Forward Checking}.
$<$\url{https://github.com/armon/cse473-ai-csp}$>$.
Retrived 2014-03-31. 
% ---- ----

\bibitem{brute_force} Weisstein, Eric W. \emph{Exhaustive Search}. From MathWorld--A Wolfram Web Resource. 
$<$\url{http://mathworld.wolfram.com/ExhaustiveSearch.html}$>$. Retrived 2014-03-30. 

\bibitem{karp} Karp, Richard.
Richard M. Karp (1972). \emph{Reducibility Among Combinatorial Problems}. 
In R. E. Miller and J. W. Thatcher (editors). 
Complexity of Computer Computations. New York: Plenum. pp. 85–103.

\bibitem{sudoku_np} Kendall, Graham, Andrew J. Parkes, and Kristian Spoerer. 
\emph{A Survey of NP-Complete Puzzles}. 
ICGA Journal 31.1 (2008): 13-34.

\bibitem{ams_exact_cover} Austin, David. \emph{Puzzling Over Exact Cover Problem}.
$<$\url{http://www.ams.org/samplings/feature-column/fcarc-kanoodle}$>$.
Retrieved 2014-03-02.

\bibitem{satcomp} \emph{The international SAT Competitions web page}.
$<$\url{http://www.satcompetition.org/}$>$. 
Retrived 2014-04-09. 

\bibitem{generator} Vegard Hanssen. \emph{Sudoku puzzles}. 
$<$\url{http://www.menneske.no/sudoku/eng/}$>$. 
Retrieved 2014-03-28.

\end{thebibliography}

\pagebreak 
\begin{appendices}
\section{Time distributions} \label{sec:timedist}
\emph{ 
    In this section we show the time distribution to solve for all 
    algorithms, sizes, and puzzles. The red line shows the median value. 
    Do note the exponent on some of the graphs. 
}

\vfill 
\begin{figure}[h]
 \centering 
 \includegraphics[height=\textheight/2]{graphs/dist_1.eps}
 \caption*{Algorithm 1: Brute-force}
\end{figure}
\vfill 

\begin{figure}
 \centering 
 \includegraphics[height=\textheight/2]{graphs/dist_2.eps}
 \caption*{Algoritm 2: Pen-and-paper}
\end{figure}

\begin{figure}
 \centering 
 \includegraphics[height=\textheight/2]{graphs/dist_3.eps}
 \caption*{Algoritm 3: Exact Cover Reduction (Haskell)}
\end{figure}

\begin{figure}
 \centering 
 \includegraphics[height=\textheight/2]{graphs/dist_4.eps}
 \caption*{Algoritm 4: Exact Cover Reduction (Python)}
\end{figure}

\begin{figure}
 \centering 
 \includegraphics[height=\textheight/2]{graphs/dist_5.eps}
 \caption*{Algoritm 5: SAT Reduction}
\end{figure}

\begin{figure}
 \centering 
 \includegraphics[height=\textheight/2]{graphs/dist_6.eps}
 \caption*{Algoritm 6: Constraint Satisfaction Problem}
\end{figure}


\newpage 
\section{Hardware specifications} \label{sec:hw}
\begin{table}[h]
\centering
\begin{tabular}{l l} 
Architecture:        &  x86\_64 \\ 
CPU op-mode(s):      &  32-bit, 64-bit \\ 
Byte Order:          &  Little Endian \\ 
CPU(s):              &  8 \\ 
On-line CPU(s) list: &  0-7 \\ 
Thread(s) per core:  &  2 \\ 
Core(s) per socket:  &  4 \\ 
Socket(s):           &  1 \\ 
NUMA node(s):        &  1 \\ 
Vendor ID:           &  GenuineIntel \\ 
CPU family:          &  6 \\ 
Model:               &  60 \\ 
Model name:          &  Intel(R) Core(TM) i7-4770 CPU @ 3.40GHz \\ 
Stepping:            &  3 \\ 
CPU MHz:             &  3760.984 \\ 
CPU max MHz:         &  3900.0000 \\ 
CPU min MHz:         &  800.0000 \\ 
BogoMIPS:            &  6800.82 \\ 
Virtualization:      &  VT-x \\ 
L1d cache:           &  32K \\ 
L1i cache:           &  32K \\ 
L2 cache:            &  256K \\ 
L3 cache:            &  8192K \\ 
NUMA node0 CPU(s):   &  0-7  
\end{tabular}
\caption{The computer specifications of the computer (from lscpu)}
\end{table}

\newpage 
\section{Code} 
\subsection{Brute-force} \label{subsec:brute}
\verbatimtabinput[4]{../algorithm1/brute-force.py}

%\subsection{Sudoku scraper}
%\verbatimtabinput[4]{../scraper/scratchpad_scraper.js}

%\subsection{Sudoku converters}
%\subsubsection{build.sh} 
%\verbatimtabinput[4]{../converter/build.sh}

%\subsubsection{run.sh}
%\verbatimtabinput[4]{../converter/run.sh}
%
%\subsubsection{ImmutableSudoku.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/ImmutableSudoku.java}
%
%\subsubsection{SimpleSudoku.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/SimpleSudoku.java}
%
%\subsubsection{Sudoku.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/Sudoku.java}
%
%\subsubsection{SudokuConverter.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/SudokuConverter.java}
%
%\subsubsection{SudokuConverterMain.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/SudokuConverterMain.java}
%
%\subsubsection{SudokuLoader.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/SudokuLoader.java}
%
%\subsubsection{Impl2Converter.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/converter/Impl2Converter.java}
%
%\subsubsection{Impl3Converter.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/converter/Impl3Converter.java}
%
%\subsubsection{Impl5Converter.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/converter/Impl5Converter.java}
%
%\subsubsection{Impl6Converter.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/converter/Impl6Converter.java}
%
%\subsubsection{DELoader.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/loader/DELoader.java}
%
%\subsubsection{LineLoader.java}
%\verbatimtabinput[4]{../converter/src/sudokuconverter/loader/LineLoader.java}


%\section{Sudokus} \label{sec:sudokus}
%\subsection{4x4}
%\input{../puzzles/n2d1.tex}

%\subsection{9x9}
%\input{../puzzles/n3d3.tex}

%\subsection{16x16}
%\input{../puzzles/n4d3.tex}

%\subsection{25x25}
%\input{../puzzles/n5d3.tex}

\end{appendices}

%1: vanlig brute-force

%2:
%http://waltertech426.blogspot.se/2013/07/matlab-sudoku-solver-in-fastest-way.html
%Matlab, löser med pen-and-paper

%3:
%http://alitarhini.wordpress.com/2012/02/27/parallel-sudoku-solver-algorithm/
%Parallelliserad lösning. Finns ingen kod?

%4: %http://www.haskell.org/haskellwiki/Sudoku#Generalized_solver Haskell,
%löser med reduktion till Exact Cover.

%5: http://efxa.org/2013/12/25/nature-inspired-sudoku-solver/ Python 2,
%löser med genetiska algoritmer.

%6: https://github.com/armon/cse473-ai-csp Java. AI-backtracking
%sak. Constraint Satisfaction Problem.

%7: http://robert.rsa3.com/sudoku.html C++, "basic error minimization search"

%8: %http://www.codeproject.com/Articles/34403/Sudoku-as-a-CSP %C#,
%Constraint Satisfaction Problem (Nope!) 

%9:
%http://codereview.stackexchange.com/questions/37415/sudoku-using-exact-cover-solver
%Python 3, reduktion till Exact Cover

\end{document}


% Todos: 
% * Mer konverteringar 
%  - till algoritm 2 (färdig) 
%  - till algoritm 4 (färdig) 
%  - till algoritm 5 (färdig) 
%  - till algoritm 6 (färdig) 
%  - till algoritm 7 (färdig) 
% * Fixa todos i rapporten 
%
% * Skriva bakgrund om 
%  - algoritm 5 
%  - algoritm 6 
%  - algoritm 7 
%
% * Skriva metodik (färdig) 
% * Lära oss om screen/tmux/nohangup-sak 
% * Hitta en snabbare genererare samt fundera över att generera 
%   andra storlekar än n^2 x n^2. Att generera 25x25 känns omöjligt, 
%   och att ens lösa 16x16 känns också omöligt. Alex nämnde ju någon 6x6-storlek.  
%   Tror inte heller att vi ska hänga upp oss på att nödvändigtvis ha samma antal 
%   pussel i varje svårighetsgrad. Duger nog med 500 i vissa och kanske 50-100
%   i andra.  (färdig) 
% * Skapa en scraper som kan hämta sudokus (färdig) 
% * Få bruteforcern att fungera med pypy (färdig) 
% * Göra en mockup om hur grafer kan se ut (med uppskattade värden) (färdig) 
% * Maila Alex om att träffas (färdig) 
% * Hitta en algoritm som löser det med reduktion till (3CNF-)SAT?  
% * Köra skiten 









