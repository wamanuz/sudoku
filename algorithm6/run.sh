for i in {0..24}
do
        echo "Puzzle #$i"
        time timeout 10800 java -jar dist/Impl6.jar "impl6/puzzle_n5d5_$i.txt"
        echo "Exit code: $?"
done

echo 'Algoritm 6' | mailx -s 'Sudokukörning klar' mpalo@kth.se
